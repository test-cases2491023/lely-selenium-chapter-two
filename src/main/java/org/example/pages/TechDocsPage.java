package org.example.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.io.File;

public class TechDocsPage {
    private WebDriver driver;
    private By catalogDropdownArea = By.id("id_q");
    private By cookieNoticeButton = By.id("cookienotice-button-accept");
    private By viewDocumentLink = By.xpath("(//a[contains(text(),'View this document')])[1]");
    private By downloadDocumentButton = By.xpath("(//a[@class='button button-secondary icon-pdf'])[2]");

    public TechDocsPage(WebDriver driver) {
        this.driver = driver;
    }

    public void navigateToURL(String url) {
        driver.get(url);
        WebElement cookies = driver.findElement(cookieNoticeButton);
        cookies.click();
    }

    public void selectCatalog(String searchText) {

        WebElement dropdownElement = driver.findElement(catalogDropdownArea);
        Select dropdown = new Select(dropdownElement);
        dropdown.selectByVisibleText(searchText);

    }

    public void verifySelectedCatalog() {

        WebElement lunaCatalog = driver.findElement(By.className("result-item-title"));
        String lunaCatalogName = lunaCatalog.getText();

        if (lunaCatalogName.contains("LUNA EUR")) {
            System.out.println("Catalog Verified: " + lunaCatalogName);
        } else {
            System.out.println("Catalog Not Verified");
        }
    }

    public void openDocumentInNewTab() {

        WebElement documentLink = driver.findElement(viewDocumentLink);
        documentLink.click();
    }

    public void verifiedToNewTab() {
        String expectedUrl = "https://www.lelynet.com/_layouts/15/document/TechDocHandler.aspx?name=D-S006VT_-.pdf&mode=view";

        String currentTab = driver.getWindowHandle();
        for (String tab : driver.getWindowHandles()) {
            if (!tab.equals(currentTab)) {
                driver.switchTo().window(tab);
                break;
            }
        }
        String actualUrl = driver.getCurrentUrl();
        assert actualUrl.equals(expectedUrl) : "URL mismatch: Expected " + expectedUrl + " but got " + actualUrl;
    }

    public void switchToPreviousTab() {
        String currentTab = driver.getWindowHandle();
        for (String tab : driver.getWindowHandles()) {
            if (!tab.equals(currentTab)) {
                driver.close();
                driver.switchTo().window(tab);
                break;
            }
        }
    }

    public void downloadDocument() throws InterruptedException {


        WebElement downloadButton = driver.findElement(downloadDocumentButton);
        downloadButton.click();

        Thread.sleep(20000);

    }

    public boolean verifyDownloadedDocument() {


        String downloadFilepath = System.getProperty("user.dir") + File.separator + "downloads";
        File folder = new File(downloadFilepath);
        File[] fileList = folder.listFiles();
        boolean isFilePresent = false;
        for (File file : fileList) {
            if (file.isFile()) {
                String fileName = file.getName();
                if (fileName.contains("D-S032VT_-")) {
                    isFilePresent = true;
                }
            }
        }
        return isFilePresent;

    }

}

