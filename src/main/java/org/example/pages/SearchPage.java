package org.example.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.time.Duration;
import java.util.List;

public class SearchPage {
    private WebDriver driver;
    private By searchButton = By.xpath("//div[@class='header-navigation-button']");
    private By searchInput = By.xpath("//input[@id='global-search']");
    private By searchWordButton = By.xpath("//button[@class='button button-tertiary']");
    private By allTextResults = By.xpath("//ul[@class='item-list search-results-list']");

    public SearchPage(WebDriver driver) {
        this.driver = driver;
    }


    public void clickSearchButton() {
        driver.findElement(searchButton).click();
    }

    public void clickSearchWordButton() {
        driver.findElement(searchWordButton).click();
    }

    public void waitSearchText() {

        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5));
        wait.until(ExpectedConditions.visibilityOfElementLocated(searchInput));
    }

    public void enterSearchText(String searchText) {

        WebElement searchInputField = driver.findElement(searchInput);
        searchInputField.clear();
        searchInputField.sendKeys(searchText);
    }

    public void verifyContainsResults() {
        List<WebElement> descriptions = driver.findElements(allTextResults);
        for (int i = 0; i < descriptions.size(); i++) {
            Assert.assertTrue(descriptions.get(i).getText().contains("happy"), "Search result validation failed at instance [ + i + ].");
        }

    }
}
