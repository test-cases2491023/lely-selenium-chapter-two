package org.example.constants;

public class Url {

    public static final String BASE_URL = "https://www.lely.com/en";
    public static final String DOCS_BASE_URL ="https://www.lely.com/techdocs/";

}
