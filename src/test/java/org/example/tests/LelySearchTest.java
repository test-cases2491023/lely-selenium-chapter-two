package org.example.tests;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.example.constants.Url;
import org.example.pages.SearchPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class LelySearchTest {
    private WebDriver driver;
    private SearchPage homePage;


    @BeforeClass
    public void setUp() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.get(Url.BASE_URL);

        homePage = new SearchPage(driver);
    }


    @Test()
    public void searchWithKeyword() {
        homePage.clickSearchButton();
        homePage.waitSearchText();
        homePage.enterSearchText("happy");
        homePage.clickSearchWordButton();
        homePage.verifyContainsResults();

    }

    @AfterClass
    public void tearDown() {
        if (driver != null) {
            driver.quit();
        }
    }
}
