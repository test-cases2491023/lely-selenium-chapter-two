package org.example.tests;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.example.constants.Url;
import org.example.pages.TechDocsPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.File;
import java.util.HashMap;

public class TechDocsTest {

    private WebDriver driver;
    private TechDocsPage lelyTechDocsPage;

    @BeforeMethod
    public void setUp() {
        WebDriverManager.chromedriver().setup();

        ChromeOptions options = new ChromeOptions();
        HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
        chromePrefs.put("profile.default_content_settings.popups",0);
        String downloadFilepath = System.getProperty("user.dir") + File.separator + "downloads";
        chromePrefs.put("download.default_directory", downloadFilepath);
        options.setExperimentalOption("prefs",chromePrefs);


        driver = new ChromeDriver(options);
        driver.manage().window().maximize();
        lelyTechDocsPage = new TechDocsPage(driver);

    }

    @Test
    public void testDownloadLunaEurCatalog() throws InterruptedException {
        lelyTechDocsPage.navigateToURL(Url.DOCS_BASE_URL);
        lelyTechDocsPage.selectCatalog("Luna EUR");
        lelyTechDocsPage.verifySelectedCatalog();
        lelyTechDocsPage.openDocumentInNewTab();
        lelyTechDocsPage.verifiedToNewTab();
        lelyTechDocsPage.switchToPreviousTab();
        lelyTechDocsPage.downloadDocument();
        Assert.assertTrue(lelyTechDocsPage.verifyDownloadedDocument());
    }

    @AfterMethod
    public void tearDown() {
        driver.quit();
    }
}
